import {
  Component,
  AfterViewInit,
  ViewChild,
  Input,
  ElementRef,
} from '@angular/core';
import {
  PhotoEditorSDKUI,
  EditorApi
} from 'photoeditorsdk/no-polyfills';
import {
  UIEvent,
  ImageFormat,
  ExportFormat
} from 'photoeditorsdk';
// SERVICES
import {
  PhotoService
} from '../services/photo/photo.service';

const license = '';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private _photoProvider: PhotoService
  ) {

  }

  @Input()
  public src: string;

  @ViewChild('psdkContainer')
  private container: ElementRef;

  public editor: EditorApi;

  ngAfterViewInit() {
    this.initEditor();
  }

  async initEditor() {
    try {
      if (this.editor) {
        this.editor.dispose();
      }

      setTimeout(() => {
        PhotoEditorSDKUI.init({
          license,
          container: this.container.nativeElement,
          image: '/assets/manuel.jpeg',
          assetBaseUrl: '/assets/photoeditorsdk/assets',
          export: {
            image: {
              enableDownload: true,
              format: ImageFormat.PNG,
              exportType: ExportFormat.DATA_URL,
            },
          },
        }).then((editor) => {
          editor.on(UIEvent.EXPORT, async (image) => {
            console.log(image);
            this.imgSync(image, 1);
          })
        })
      }, 2000);

    } catch (error) {
      console.log(error);
    }
  }

  async imgSync(myphoto, codigoAlbum) {
    let photo = await this._photoProvider.subirFoto(myphoto, codigoAlbum);
    if (photo) {
      console.log('Album subido');
    } else {
      console.log('error album');
    }
    return;
  }

  // async initEditor() {
  //   try {
  //     if (this.editor) {
  //       this.editor.dispose();
  //     }

  //     this.editor = await PhotoEditorSDKUI.init({
  //       license,
  //       container: this.container.nativeElement,
  //       image: '/assets/manuel.jpeg',
  //       assetBaseUrl: '/assets/photoeditorsdk/assets',
  //       export: {
  //         image: {
  //           enableDownload: true,
  //           format: ImageFormat.JPEG,
  //           exportType: ExportFormat.DATA_URL,
  //         },
  //       },
  //     }).then((editor) =>{
  //       //       editor.on(UIEvent.EXPORT, async (image) =>{
  //       //         console.log(image);
  //       //         this.imgSync(image, 1);
  //       //       })
  //           })
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
}
