// PLUGINS
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from '@ionic-native/file-transfer/ngx';

import {
  Observable,
  throwError
} from 'rxjs';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  map
} from 'rxjs/operators';

import {
  Injectable
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  status: boolean;

  constructor(
    public http: HttpClient,
    private transfer: FileTransfer,
  ) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error has been occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

    // subir foto 

    async subirFoto(firmab64: string, codigoAlbum: number) {

      // Creando archivo de objeto de tranferencia
      const fileTransfer: FileTransferObject = this.transfer.create();
  
      let params = {
        album: codigoAlbum
      }
  
      //Opciones de Tranferencia
      let options: FileUploadOptions = {
        fileKey: 'image',
        chunkedMode: false,
        httpMethod: 'post',
        mimeType: "image/png",
        headers: {},
        params
      }
  
      //Creando tranferencia
      await fileTransfer.upload(firmab64, 'https://magicbook15.asms.gt/SISTEM'  + "/API/API_photos_upload.php?album", options)
        .then((data) => {
          this.status = true;
          console.log(data)
        }, (err) => {
          this.status = false;
        });
        return this.status;
    }
}
